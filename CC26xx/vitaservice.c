/*******************************************************************************
  Filename:       vitaservice.c
  Revised:
  Revision:

  Description:    Vita service.


  Copyright 2012 - 2015 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
*******************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "linkdb.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "string.h"

#include "vitaservice.h"
#include "st_util.h"
#include "vita_uart.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

/* Service configuration values */
#define SENSOR_SERVICE_UUID     VITA_SERV_UUID
#define SENSOR_DATA_UUID        VITA_DATA_UUID
#define SENSOR_CONFIG_UUID      VITA_CONF_UUID
#define SENSOR_DEBUG_UUID       VITA_DEBUG_UUID
#define SENSOR_DEBUG_CONF_UUID  VITA_DEBUG_CONF_UUID

#define SENSOR_SERVICE          VITA_SERVICE
#define SENSOR_DATA_LEN         VITA_DATA_LEN
#define SENSOR_CONF_LEN         VITA_CONF_LEN
#define SENSOR_DEBUG_LEN        VITA_DEBUG_LEN
#define SENSOR_DEBUG_CONF_LEN   VITA_DEBUG_CONF_LEN

#define SENSOR_DATA_DESCR       "VITA Data"
#define SENSOR_CONFIG_DESCR     "VITA Conf."
#define SENSOR_DEBUG_DESCR      "VITA Debug"
#define SENSOR_DEBUG_CONF_DESCR "VITA Dbg Cnf"

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

// Service UUID
static CONST uint8_t sensorServiceUUID[TI_UUID_SIZE] =
{
  TI_UUID(SENSOR_SERVICE_UUID),
};

// Characteristic UUID: data
static CONST uint8_t sensorScanDataUUID[TI_UUID_SIZE] =
{
  TI_UUID(SENSOR_DATA_UUID),
};

// Characteristic UUID: config
static CONST uint8_t sensorCfgUUID[TI_UUID_SIZE] =
{
  TI_UUID(SENSOR_CONFIG_UUID),
};

// Characteristic UUID: debug
static CONST uint8_t sensorDebugDataUUID[TI_UUID_SIZE] =
{
  TI_UUID(SENSOR_DEBUG_UUID),
};

// Characteristic UUID: debug
static CONST uint8_t sensorDebugConfUUID[TI_UUID_SIZE] =
{
  TI_UUID(SENSOR_DEBUG_CONF_UUID),
};


/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static sensorCBs_t *sensor_AppCBs = NULL;

/*********************************************************************
 * Profile Attributes - variables
 */

// Profile Service attribute
static CONST gattAttrType_t sensorService = { TI_UUID_SIZE, sensorServiceUUID };

// Characteristic Value: data
static uint8_t sensorScanData[SENSOR_DATA_LEN] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// Characteristic Properties: data
static uint8_t sensorScanDataProps = GATT_PROP_READ | GATT_PROP_NOTIFY;

// Characteristic Configuration: data
static gattCharCfg_t *sensorScanDataConfig;

#ifdef USER_DESCRIPTION
// Characteristic User Description: data
static uint8_t sensorScanDataUserDescr[] = SENSOR_DATA_DESCR;
#endif

// Characteristic Properties: configuration
static uint8_t sensorCfgProps = GATT_PROP_READ | GATT_PROP_WRITE;

// Characteristic Value: configuration
static uint8_t sensorCfg[SENSOR_CONF_LEN] = {0};

#ifdef USER_DESCRIPTION
// Characteristic User Description: configuration
static uint8_t sensorCfgUserDescr[] = SENSOR_CONFIG_DESCR;
#endif

// Characteristic Value: debug
static uint8_t sensorDebugData[SENSOR_DEBUG_LEN] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// Characteristic Properties: debug
static uint8_t sensorDebugDataProps = GATT_PROP_READ | GATT_PROP_NOTIFY;

// Characteristic Configuration: debug
static gattCharCfg_t *sensorDebugDataConfig;

#ifdef USER_DESCRIPTION
// Characteristic User Description: debug
static uint8_t sensorDebugDataUserDescr[] = SENSOR_DEBUG_DESCR;
#endif

// Characteristic Properties: debug configuration
static uint8_t sensorDebugConfProps = GATT_PROP_READ | GATT_PROP_WRITE;

// Characteristic Value: debug configuration
static uint8_t sensorDebugConf[SENSOR_DEBUG_CONF_LEN] = {0};

#ifdef USER_DESCRIPTION
// Characteristic User Description: debug configuration
static uint8_t sensorDebugConfUserDescr[] = SENSOR_DEBUG_CONF_DESCR;
#endif

/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t sensorAttrTable[] =
{
  {
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                         /* permissions */
    0,                                        /* handle */
    (uint8_t *)&sensorService                 /* pValue */
  },

    //DATA
    // Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &sensorScanDataProps
    },

      // Characteristic Value "Data"
      {
        { TI_UUID_SIZE, sensorScanDataUUID },
        GATT_PERMIT_READ,
        0,
        sensorScanData
      },

      // Characteristic configuration
      {
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        (uint8_t *)&sensorScanDataConfig
      },

#ifdef USER_DESCRIPTION
      // Characteristic User Description
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        sensorScanDataUserDescr
      },
#endif

	//CONFIG
    // Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &sensorCfgProps
    },

      // Characteristic Value "Configuration"
      {
        { TI_UUID_SIZE, sensorCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        sensorCfg
      },

#ifdef USER_DESCRIPTION
      // Characteristic User Description
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        sensorCfgUserDescr
      },
#endif

	//DEBUG
    // Characteristic Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &sensorDebugDataProps
    },

      // Characteristic Value "Data"
      {
        { TI_UUID_SIZE, sensorDebugDataUUID },
        GATT_PERMIT_READ,
        0,
        sensorDebugData
      },

      // Characteristic configuration
      {
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        (uint8_t *)&sensorDebugDataConfig
      },

#ifdef USER_DESCRIPTION
      // Characteristic User Description
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        sensorDebugDataUserDescr
      },
#endif

		//DEBUG CONFIG
	    // Characteristic Declaration
	    {
	      { ATT_BT_UUID_SIZE, characterUUID },
	      GATT_PERMIT_READ,
	      0,
	      &sensorDebugConfProps
	    },

	      // Characteristic Value "Configuration"
	      {
	        { TI_UUID_SIZE, sensorDebugConfUUID },
	        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
	        0,
			sensorDebugConf
	      },

	#ifdef USER_DESCRIPTION
	      // Characteristic User Description
	      {
	        { ATT_BT_UUID_SIZE, charUserDescUUID },
	        GATT_PERMIT_READ,
	        0,
			sensorDebugConfUserDescr
	      },
	#endif

};


/*********************************************************************
 * LOCAL FUNCTIONS
 */
static bStatus_t sensor_ReadAttrCB(uint16_t connHandle, gattAttribute_t *pAttr,
                                   uint8_t *pValue, uint16_t *pLen,
                                   uint16_t offset, uint16_t maxLen,
                                   uint8_t method);
static bStatus_t sensor_WriteAttrCB(uint16_t connHandle, gattAttribute_t *pAttr,
                                    uint8_t *pValue, uint16_t len,
                                    uint16_t offset, uint8_t method);

/*********************************************************************
 * PROFILE CALLBACKS
 */
// Vita Profile Service Callbacks
static CONST gattServiceCBs_t sensorCBs =
{
  sensor_ReadAttrCB,  // Read callback function pointer
  sensor_WriteAttrCB, // Write callback function pointer
  NULL                // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      Vita_addService
 *
 * @brief   Initializes the Vita Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @return  Success or Failure
 */
bStatus_t Vita_addService(void)
{
  // DATA
  // Allocate Client Characteristic Configuration table
  sensorScanDataConfig = (gattCharCfg_t *)ICall_malloc(sizeof(gattCharCfg_t) *
                                                    linkDBNumConns);
  if (sensorScanDataConfig == NULL)
  {
    return (bleMemAllocError);
  }

  // Register with Link DB to receive link status change callback
  //GATTServApp_InitCharCfg(INVALID_CONNHANDLE, sensorScanDataConfig);

  // DEBUG
  // Allocate Client Characteristic Configuration table
  sensorDebugDataConfig = (gattCharCfg_t *)ICall_malloc(sizeof(gattCharCfg_t) *
                                                    linkDBNumConns);
  if (sensorDebugDataConfig == NULL)
  {
    return (bleMemAllocError);
  }

  // Register with Link DB to receive link status change callback
  GATTServApp_InitCharCfg(INVALID_CONNHANDLE, sensorDebugDataConfig);

  // Register GATT attribute list and CBs with GATT Server App
  return GATTServApp_RegisterService(sensorAttrTable,
                                     GATT_NUM_ATTRS (sensorAttrTable),
                                     GATT_MAX_ENCRYPT_KEY_SIZE,
                                     &sensorCBs);
}


/*********************************************************************
 * @fn      Vita_registerAppCBs
 *
 * @brief   Registers the application callback function. Only call
 *          this function once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t Vita_registerAppCBs(sensorCBs_t *appCallbacks)
{
  if (sensor_AppCBs == NULL)
  {
    if (appCallbacks != NULL)
    {
      sensor_AppCBs = appCallbacks;
    }

    return (SUCCESS);
  }

  return (bleAlreadyInRequestedMode);
}

/*********************************************************************
 * @fn      Vita_setParameter
 *
 * @brief   Set a parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to write
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16_t will be cast to
 *          uint16_t pointer).
 *
 * @return  bStatus_t
 */
bStatus_t Vita_setParameter(uint8_t param, uint8_t len, void *value)
{
  bStatus_t ret = SUCCESS;

  switch (param)
  {
    case SENSOR_DATA:
    if (len == SENSOR_DATA_LEN)
    {
      memcpy(sensorScanData, value, SENSOR_DATA_LEN);
      // See if Notification has been enabled
      ret = GATTServApp_ProcessCharCfg(sensorScanDataConfig, sensorScanData, FALSE,
                                 sensorAttrTable, GATT_NUM_ATTRS (sensorAttrTable),
                                 INVALID_TASK_ID, sensor_ReadAttrCB);
    }
    else
    {
      ret = bleInvalidRange;
    }
    break;

    case SENSOR_CONF:
      if (len == SENSOR_CONF_LEN)
      {
        memcpy(sensorCfg, value, SENSOR_CONF_LEN);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case SENSOR_DEBUG:
        if (len == SENSOR_DEBUG_LEN)
        {
          memcpy(sensorDebugData, value, SENSOR_DEBUG_LEN);
          // See if Notification has been enabled
          ret = GATTServApp_ProcessCharCfg(sensorDebugDataConfig, sensorDebugData, FALSE,
                                     sensorAttrTable, GATT_NUM_ATTRS (sensorAttrTable),
                                     INVALID_TASK_ID, sensor_ReadAttrCB);
        }
        else
        {
          ret = bleInvalidRange;
        }
        break;

    case SENSOR_DEBUG_CONF:
        if (len == SENSOR_DEBUG_CONF_LEN)
        {
          memcpy(sensorDebugConf, value, SENSOR_DEBUG_CONF_LEN);
        }
        else
        {
          ret = bleInvalidRange;
        }
        break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return (ret);
}

/*********************************************************************
 * @fn      Vita_getParameter
 *
 * @brief   Get a Sensor Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16_t will be cast to
 *          uint16_t pointer).
 *
 * @return  bStatus_t
 */
bStatus_t Vita_getParameter(uint8_t param, void *value)
{
  bStatus_t ret = SUCCESS;

  switch (param)
  {
    case SENSOR_DATA:
      memcpy(value, sensorScanData, SENSOR_DATA_LEN);
      break;

    case SENSOR_CONF:
    	memcpy(value, sensorCfg, SENSOR_CONF_LEN);
      break;

    case SENSOR_DEBUG:
    	memcpy(value, sensorDebugData, SENSOR_DEBUG_LEN);
      break;

    case SENSOR_DEBUG_CONF:
    	memcpy(value, sensorDebugConf, SENSOR_DEBUG_CONF_LEN);
      break;


    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return (ret);
}

/*********************************************************************
 * @fn          sensor_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 * @param       method - type of read message
 *
 * @return      SUCCESS, blePending or Failure
 */
static bStatus_t sensor_ReadAttrCB(uint16_t connHandle, gattAttribute_t *pAttr,
                                   uint8_t *pValue, uint16_t *pLen,
                                   uint16_t offset, uint16_t maxLen,
                                   uint8_t method)
{
  uint16_t uuid;
  bStatus_t status = SUCCESS;

  // If attribute permissions require authorization to read, return error
  if (gattPermitAuthorRead(pAttr->permissions))
  {
    // Insufficient authorization
    return (ATT_ERR_INSUFFICIENT_AUTHOR);
  }

  // Make sure it's not a blob operation (no attributes in the profile are long)
  if (offset > 0)
  {
    return (ATT_ERR_ATTR_NOT_LONG);
  }

  if (utilExtractUuid16(pAttr,&uuid) == FAILURE)
  {
    // Invalid handle
    *pLen = 0;
    return ATT_ERR_INVALID_HANDLE;
  }

  switch (uuid)
  {
    // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
    // gattserverapp handles those reads
    case SENSOR_DATA_UUID:
      *pLen = SENSOR_DATA_LEN;
      memcpy(pValue, pAttr->pValue, SENSOR_DATA_LEN);
      break;

    case SENSOR_CONFIG_UUID:
      *pLen = SENSOR_CONF_LEN;
      memcpy(pValue, pAttr->pValue, SENSOR_CONF_LEN);
      break;

    case SENSOR_DEBUG_UUID:
      *pLen = SENSOR_DEBUG_LEN;
      memcpy(pValue, pAttr->pValue, SENSOR_DEBUG_LEN);
      break;

    case SENSOR_DEBUG_CONF_UUID:
      *pLen = SENSOR_DEBUG_CONF_LEN;
      memcpy(pValue, pAttr->pValue, SENSOR_DEBUG_CONF_LEN);
      break;

    default:
      *pLen = 0;
      status = ATT_ERR_ATTR_NOT_FOUND;
      break;
    }

  return (status);
}

/*********************************************************************
 * @fn      sensor_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   method - type of write message
 *
 * @return  SUCCESS, blePending or Failure
 */
static bStatus_t sensor_WriteAttrCB(uint16_t connHandle, gattAttribute_t *pAttr,
                                    uint8_t *pValue, uint16_t len,
                                    uint16_t offset, uint8_t method)
{
  bStatus_t status = SUCCESS;
  uint8_t notifyApp = 0xFF;
  uint16_t uuid;

  // If attribute permissions require authorization to write, return error
  if (gattPermitAuthorWrite(pAttr->permissions))
  {
    // Insufficient authorization
    return (ATT_ERR_INSUFFICIENT_AUTHOR);
  }

  if (utilExtractUuid16(pAttr,&uuid) == FAILURE) {
    // Invalid handle
    return ATT_ERR_INVALID_HANDLE;
  }

  switch (uuid)
  {
    case SENSOR_DATA_UUID:
    case SENSOR_DEBUG_UUID:
      // Should not get here
      break;

    case SENSOR_CONFIG_UUID:
      // Validate the value
      // Make sure it's not a blob oper
      if (offset == 0)
      {
        if (len != SENSOR_CONF_LEN)
        {
          status = ATT_ERR_INVALID_VALUE_SIZE;
        }
      }
      else
      {
        status = ATT_ERR_ATTR_NOT_LONG;
      }

      // Write the value
      if (status == SUCCESS)
      {
         memcpy(pAttr->pValue, pValue, SENSOR_CONF_LEN);

         if (pAttr->pValue == (uint8_t*)&sensorCfg)
         {
            notifyApp = SENSOR_CONF;
         }
       }

      break;

    case SENSOR_DEBUG_CONF_UUID:
        // Validate the value
        // Make sure it's not a blob oper
        if (offset == 0)
        {
          if (len != SENSOR_DEBUG_CONF_LEN)
          {
            status = ATT_ERR_INVALID_VALUE_SIZE;
          }
        }
        else
        {
          status = ATT_ERR_ATTR_NOT_LONG;
        }

        // Write the value
        if (status == SUCCESS)
        {
           memcpy(pAttr->pValue, pValue, SENSOR_DEBUG_CONF_LEN);

           if (pAttr->pValue == (uint8_t*)&sensorDebugConf)
           {
              notifyApp = SENSOR_CONF;
           }
         }

        break;

    case GATT_CLIENT_CHAR_CFG_UUID:
      status = GATTServApp_ProcessCCCWriteReq(connHandle, pAttr, pValue, len,
                                              offset, GATT_CLIENT_CFG_NOTIFY);
      break;

    default:
      // Should never get here!
      status = ATT_ERR_ATTR_NOT_FOUND;
      break;
  }

  // If a characteristic value changed then callback
  // function to notify application of change
  if ((notifyApp != 0xFF ) && sensor_AppCBs && sensor_AppCBs->pfnSensorChange)
  {
    sensor_AppCBs->pfnSensorChange(notifyApp);
  }

  return (status);
}

/*********************************************************************
*********************************************************************/
